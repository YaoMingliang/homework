import random
from itertools import count
total = 0 #同花顺的次数

def all_cards(): # 函数返回一副扑克除去大小王的剩余52张牌
    num = list(range(2,11))
    num.extend("JQKA")
    suits = ["h","d","s","c"]
    all_card = [(m,str(n)) for n in num for m in suits]#m在花色中迭代,n在牌面中迭代
    return all_card


num_dict = {'2':2,'3':3,'4':4,'5':5,'6':6,'7':7,'8':8,'9':9,'10':10,'J':11,'Q':12,'K':13,'A':14}#为方便排序,将牌映射成数字

def fapai(): # 发牌,从52张牌中随机选取13张牌,并把这13张牌从总牌里面删去,以免造成重复牌
    a = [] 
    cards = all_cards() # 生成一幅牌
    for i in range(13):
        b = random.choice(cards)
        a.append(b)
        cards.remove(b)
    return a

for g in range(10000):
    b = fapai()
    a = fapai()
    boo = Flase # 判断牌中有没有同花顺
    final_h = []
    final_d = []
    final_s = []
    final_c = []
    for item in a:
        if item[0] == 'h':
            final_h.append(num_dict[item[1]])
        elif item[0] == 'd':
            final_d.append(num_dict[item[1]])
            # pass
        elif item[0] == 's':
            final_s.append(num_dict[item[1]])
            # pass
        elif item[0] == 'c':
            final_c.append(num_dict[item[1]])
            # pass
    if len(final_h) >=5:
        final_h.sort()
        for x in range(len(final_h)-4):
            if int(final_h[x+4])-int(final_h[x]) == 4:# 下标相差4的话，如果牌面插4，判定为同花顺
                boo = True
            
    elif len(final_d) >= 5:
        final_d.sort()
        for x in range(len(final_d)-4):
            if int(final_d[x+4])-int(final_d[x]) == 4:
                boo = True
    elif len(final_s) >= 5:
        final_s.sort()
        for x in range(len(final_s)-4):
            if int(final_s[x+4])-int(final_s[x]) == 4:
                boo = True
    elif len(final_c) >= 5:
        final_c.sort()
        for x in range(len(final_c)-4):
            if int(final_c[x+4])-int(final_c[x]) == 4:
                boo = True
    if boo == True:
        total += 1
print("发牌10000次出现同花顺的概率为:",total/10000)
