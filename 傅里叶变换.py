import numpy as np
import matplotlib.pyplot as plt
Fs=30
x=np.arange(0,4,1/Fs)
N = len(x)
F1=np.sin(2*np.pi*10*x)
F2=np.sin(2*np.pi*x*5)
F3=2*np.sin(2*np.pi*2*x)

freq = np.arange(N) / N * Fs
Y = np.fft.fft(F1+F2+F3)/(N/2)
freq_half = freq[range(int(N/2))]
Y_half = Y[range(int(N/2))]
fig, ax = plt.subplots(5, 1, figsize=(5, 5))
ax[0].plot(x,F1)
ax[1].plot(x,F2)
ax[2].plot(x,F3)
ax[3].plot(x,F1+F2+F3)
ax[4].plot(freq_half,abs(Y_half))
plt.show()
