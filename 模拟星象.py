
import matplotlib.pyplot as plt
import numpy as np

dist1 = np.random.normal(50, 10, 100000)
dist2 = np.random.normal(50, 10, 100000)

plt.hist2d(dist1,
            dist2,
            bins=35,
            cmap='gray')
plt.colorbar()

'''
import numpy as np
import matplotlib.pyplot as plt
'''
定义中心点处点的数量最多，
从中心点（0,0）向周围的点的数量，且呈高斯分布
'''
mean = [0,0]
cov = [[1,0],[0,1]]
data = np.random.multivariate_normal(mean, cov, 30000)

x, y = data.T
plt.hist2d(x,y, bins=50,cmap='gray')
plt.show()
'''
